AGGREGATE_ROWS=$1
TESTSERVER=$2
THREADS=$3
RAMPUP=$4
DURATION=$5
SLAVE_NODES=$6
AGENT=$7

java -jar $JMETER_HOME/../lib/ext/CMDRunner.jar --tool Reporter --generate-png TransactionsPerSecond.png --input-jtl results.jtl --plugin-type TransactionsPerSecond --aggregate-rows $AGGREGATE_ROWS --auto-scale yes --exclude-labels "Dummy HTTP Request" --relative-times no

java -jar $JMETER_HOME/../lib/ext/CMDRunner.jar --tool Reporter --generate-png SYSTEM_CPU_AGENT1.png --input-jtl perfmon_agent1.jtl --plugin-type PerfMon --auto-scale yes --include-labels "vagent01int.bvt.com CPU"  --relative-times no
java -jar $JMETER_HOME/../lib/ext/CMDRunner.jar --tool Reporter --generate-png SYSTEM_CPU_AGENT2.png --input-jtl perfmon_agent2.jtl --plugin-type PerfMon --auto-scale yes --include-labels "vagent02int.bvt.com CPU"  --relative-times no

java -jar $JMETER_HOME/../lib/ext/CMDRunner.jar --tool Reporter --generate-png SYSTEM_MEM_AGENT1.png --input-jtl perfmon_agent1.jtl --plugin-type PerfMon --auto-scale yes --include-labels "vagent01int.bvt.com Memory"  --relative-times no
java -jar $JMETER_HOME/../lib/ext/CMDRunner.jar --tool Reporter --generate-png SYSTEM_MEM_AGENT2.png --input-jtl perfmon_agent2.jtl --plugin-type PerfMon --auto-scale yes --include-labels "vagent02int.bvt.com Memory"  --relative-times no

java -jar $JMETER_HOME/../lib/ext/CMDRunner.jar --tool Reporter --generate-png NETWORK_AGENT1.png --input-jtl perfmon_agent1.jtl --plugin-type PerfMon --auto-scale yes --include-labels "vagent01int.bvt.com Network I/O"  --relative-times no
java -jar $JMETER_HOME/../lib/ext/CMDRunner.jar --tool Reporter --generate-png NETWORK_AGENT2.png --input-jtl perfmon_agent2.jtl --plugin-type PerfMon --auto-scale yes --include-labels "vagent02int.bvt.com Network I/O"  --relative-times no

AGENT1_VERSION=$(curl -s http://10.232.72.244/vormetric-adapter/internal/status/version)
AGENT2_VERSION=$(curl -s http://10.232.92.224/vormetric-adapter/internal/status/version)

sed -e "s/%TESTSERVER%/$TESTSERVER/g" vormetric-test-automation/report_template.html > report.html
sed -i -e "s/%THREADS%/$THREADS/g" report.html
sed -i -e "s/%RAMPUP%/$RAMPUP/g" report.html
sed -i -e "s/%DURATION%/$DURATION/g" report.html
sed -i -e "s/%SLAVE_NODES%/$SLAVE_NODES/g" report.html
#sed -i -e "s/%AGENT1_VERSION%/$AGENT1_VERSION/g" report.html
#sed -i -e "s/%AGENT2_VERSION%/$AGENT2_VERSION/g" report.html